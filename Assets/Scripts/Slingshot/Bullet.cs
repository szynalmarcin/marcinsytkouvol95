﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SlingShot {

    public class Bullet : MonoBehaviour, IPoolable {
        bool isCurrentBall;
        Rigidbody2D rigid;
        float stopSpeed = 0.05f;

        private void Awake() {
            rigid = GetComponent<Rigidbody2D>();
        }

        public void Release(Vector3 force) {
            rigid.isKinematic = false;
            Invoke("SetCurrentBall", 0.1f);
            rigid.AddForce(force, ForceMode2D.Force);
        }


        public void SetPosition(Vector3 pos) {
            rigid.MovePosition(pos);
        }

        public void DisableRigid() {
            rigid.isKinematic = true;
        }

        public void OnPopFromPool() {
            Debug.Log("OnPopFromPool");
            gameObject.SetActive(true);
        }

        public void OnPushIntoPool() {
            DisableRigid();
        }

        void SetCurrentBall() {
            isCurrentBall = true;
        }
        private void Update() {
            if (isCurrentBall) {
                Debug.Log(rigid.velocity.x);
                if (rigid.velocity.x < stopSpeed || rigid.IsSleeping()) {
                    rigid.velocity = Vector3.zero;
                    GameController.instance.ChangeGameState(GameController.GameState.WaitForNextShot);
                    isCurrentBall = false;
                }
            }
        }
    }
}
