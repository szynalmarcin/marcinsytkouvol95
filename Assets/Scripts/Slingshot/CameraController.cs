﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SlingShot {

    public class CameraController : MonoBehaviour {
        public GameParamsSO gameParams;

        public Vector3 offset;
        Transform target;
        Vector3 velocity;


        public void FollowTarget(Transform target) {
            this.target = target;
        }

        private void LateUpdate() {
            if (target != null) {
                transform.position = Vector3.Lerp(transform.position, target.position + offset, gameParams.followDamp);
            }
        }
    }
}
