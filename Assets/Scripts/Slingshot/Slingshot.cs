﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SlingShot {
    public class Slingshot : MonoBehaviour {

        public Camera cam;
        public GameParamsSO gameParams;

        Bullet loadedBullet;
        bool isDragged;

        public Transform aimStart;

        public void OnMouseDown() {
            if (CanDrag()) {
                isDragged = true;
            }
        }

        private void OnMouseDrag() {
            Vector3 desiredPosition = cam.ScreenToWorldPoint(Input.mousePosition);
            desiredPosition.z = 0;
            if (Vector3.Distance(desiredPosition, aimStart.position) > gameParams.maxAimDistance) {
                desiredPosition = aimStart.position + GetAimDirection(desiredPosition).normalized * gameParams.maxAimDistance;
            }
            loadedBullet.SetPosition(desiredPosition);
        }

        private Vector3 GetAimDirection(Vector3 desiredPosition) {
            return (desiredPosition - aimStart.position);
        }

        private void OnMouseUp() {
            if (isDragged) {
                isDragged = false;
                GameController.instance.ChangeGameState(GameController.GameState.Flying);
                ReleaseBullet();
            }
        }

        private void ReleaseBullet() {
            Vector3 desiredPosition = cam.ScreenToWorldPoint(Input.mousePosition);
            desiredPosition.z = 0;
            Vector3 releaseDir = GetAimDirection(desiredPosition).normalized;
            float powerMod = releaseDir.magnitude / gameParams.maxAimDistance;
            loadedBullet.Release(-releaseDir * powerMod * gameParams.maxPower);
            loadedBullet = null;
        }



        bool CanDrag() {
            return GameController.instance.CanAim();
        }

        internal void LoadBullet(Bullet bullet) {
            bullet.transform.parent = null;
            bullet.SetPosition(aimStart.position);
            loadedBullet = bullet;
        }
    }

}