﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SlingShot {

    public class GameController : MonoBehaviour {

        public enum GameState {
            NotInitialized = 0,
            Aiming = 1,
            Flying = 2,
            WaitForNextShot = 3
        }

        public static GameController instance;
        public GameParamsSO gameParams;
        public BulletPool bulletsPool;
        public Slingshot slingShot;
        public CameraController camController;
        public Transform startCamFollowPos;
        Bullet currentBullet;
        GameState gameState = GameState.NotInitialized;

        private void Awake() {
            instance = this;
        }

        private void Start() {
            ChangeGameState(GameState.Aiming);
        }

        private void LoadNextBullet() {
            currentBullet = bulletsPool.Pop();
            slingShot.LoadBullet(currentBullet);
        }

        public void ChangeGameState(GameState state) {
            Debug.Log(string.Format("ChangeGameState from {0} to {1}", gameState, state));
            gameState = state;
            switch (state) {
                case GameState.Aiming:
                LoadNextBullet();
                camController.FollowTarget(startCamFollowPos.transform);
                break;
                case GameState.Flying:
                camController.FollowTarget(currentBullet.transform);
                break;
                case GameState.WaitForNextShot:
                StartCoroutine(WaitForNextShotC());
                break;
                default:
                break;
            }
        }

        private IEnumerator WaitForNextShotC() {
            yield return new WaitForSeconds(gameParams.waitTime);
            ChangeGameState(GameState.Aiming);
        }

        internal bool CanAim() {
            return gameState == GameState.Aiming;
        }
    }

}