﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SlingShot {

    [CreateAssetMenu]
    public class GameParamsSO : ScriptableObject {

        public float maxPower;
        public float maxAimDistance;
        public float waitTime = 1f;
        public float followDamp = 0.3f;
    }
}