﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseObjectPool<T> : MonoBehaviour where T : Component, IPoolable {

    public GameObject prefab;
    public Queue<T> pooledObjects = new Queue<T>();
    public int startPoolCapacity;
    public bool loadOnStart = true;

    void Start() {
        if (loadOnStart) {
            Load(startPoolCapacity);
        }
    }

    public void Load(int count) {
        if (prefab == null) {
            Debug.Log("Prefab does not exist");
            return;
        }
        for (int i = 0; i < count; i++) {
            AddNewObject();
        }
    }



    protected T AddNewObject() {
        GameObject go = Instantiate(prefab, transform) as GameObject;
        T component = go.GetComponent<T>();
        Push(component);
        return component;
    }

    public T Pop() {
        if (pooledObjects.Count == 0) {
            AddNewObject();
        }
        T ret = pooledObjects.Dequeue();
        ret.OnPopFromPool();
        return ret;
    }

    public void Push(T obj) {
        obj.OnPushIntoPool();
        obj.gameObject.SetActive(false);
        obj.transform.parent = transform;
        pooledObjects.Enqueue(obj);
    }
}