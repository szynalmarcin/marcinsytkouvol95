﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GravityBalls {

    [CreateAssetMenu]
    public class GravityGameParamsSO : ScriptableObject {

        public float spawnTime = 0.25f;
        public int maxBalls = 250;
        public float startMass = 1;
        public float massToForceMode = 1f;
        public float gravityDistance = 3f;
        public int combineLimit = 50;
        public float splitMergeDelay = 0.5f;
        public float splitPower  = 2f;
    }
}