﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GravityBalls {

    public class GameController : MonoBehaviour {

        public static GameController instance;
        public GravityGameParamsSO gameParams;
        public GravityBallPool ballPool;
        public Camera cam;
        public Text ballCounter;
        int _spawnedBalls;
        int spawnedBalls {
            get {
                return _spawnedBalls;
            }
            set {
                _spawnedBalls = value;
                UpdateBallCounter();
            }
        }
        float lastSpawnTime = 0;

        private void Awake() {
            instance = this;
        }

        private void Update() {
            TrySpawnBall();
        }

        private void TrySpawnBall() {
            if (Time.time - lastSpawnTime < gameParams.spawnTime) {
                return;
            }
            if (spawnedBalls < gameParams.maxBalls) {
                lastSpawnTime = Time.time;
                SpawnBall();
            }
        }

        private void SpawnBall() {
            float x = UnityEngine.Random.Range(0.05f, 0.95f);
            float y = UnityEngine.Random.Range(0.05f, 0.95f);
            Vector3 pos = new Vector3(x, y, 10.0f);
            pos = cam.ViewportToWorldPoint(pos);
            GravityBall ball = ballPool.Pop();
            ball.transform.parent = null;
            ball.transform.position = pos;
            ball.EnableCollision();
            spawnedBalls++;

        }

        internal void Split(GravityBall gravityBall) {
            int splitCount = gravityBall.GetCombineCounter();
            gravityBall.DisableCollision();
            for (int i = 0; i < splitCount; i++) {
                GravityBall ball = ballPool.Pop();
                ball.transform.parent = null;
                ball.transform.position = gravityBall.transform.position;
                ball.DisableCollissionFor(gameParams.splitMergeDelay);
                ball.SetForceInRandomDirection();
            }
            ballPool.Push(gravityBall);
        }

        private void UpdateBallCounter() {
            ballCounter.text = "Spawned " + _spawnedBalls;
        }

        public void CombineBalls(GravityBall ball1, GravityBall ball2) {
            if (ball1.CanCombine() && ball2.CanCombine()) {
                ball1.CombineWith(ball2);
                Vector3 pos = (ball1.transform.position + ball2.transform.position) / 2f;
                ball1.MoveToPos(pos);
                ballPool.Push(ball2);
            }
        }
    }

}
