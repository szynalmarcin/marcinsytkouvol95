﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GravityBalls {

    public class GravityBall : MonoBehaviour, IPoolable {

        public bool canCombine = true;
        public GravityGameParamsSO gameParams;
        Rigidbody2D rigid;
        PointEffector2D effector;
        public CircleCollider2D collisionCollider;
        public CircleCollider2D effectorCollider;
        int combineCounter = 1;

        private void Awake() {
            rigid = GetComponent<Rigidbody2D>();
            effector = GetComponent<PointEffector2D>();
        }

        public int GetCombineCounter() {
            return combineCounter;
        }

        internal float GetMass() {
            return rigid.mass;
        }


        private void OnCollisionEnter2D(Collision2D collision) {
            GravityBall ball = collision.gameObject.GetComponent<GravityBall>();
            if (ball != null) {
                GameController.instance.CombineBalls(this, ball);
            }

        }



        private float GetEffectionForce() {
            return -GetMass() * gameParams.massToForceMode;
        }


        internal void CombineWith(GravityBall ball) {
            rigid.mass += ball.GetMass();
            combineCounter += ball.GetCombineCounter();
            effector.forceMagnitude = GetEffectionForce();
            if (combineCounter > gameParams.combineLimit) {
                GameController.instance.Split(this);
            }
        }


        public void OnPopFromPool() {
            canCombine = true;
            combineCounter = 1;
            rigid.mass = gameParams.startMass;
            effector.forceMagnitude = GetEffectionForce();
            effectorCollider.radius = gameParams.gravityDistance;
            gameObject.SetActive(true);
        }

        internal void EnableCollision() {
            collisionCollider.enabled = true;
            effectorCollider.enabled = true;
        }

        public void OnPushIntoPool() {
            canCombine = false;
            DisableCollision();
        }
        public void DisableCollision() {
            collisionCollider.enabled = false;
            effectorCollider.enabled = false;
        }
        internal void DisableCollissionFor(float delay) {
            DisableCollision();
            Invoke("EnableCollision", delay);
        }

        internal void SetForceInRandomDirection() {
            Vector2 dir = UnityEngine.Random.insideUnitCircle * gameParams.splitPower;
            rigid.AddForce(dir, ForceMode2D.Force);
        }

        internal bool CanCombine() {
            return canCombine;
        }

        internal void MoveToPos(Vector3 pos) {
            rigid.MovePosition(pos);
        }
    }
}